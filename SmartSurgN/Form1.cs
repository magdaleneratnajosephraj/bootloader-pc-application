﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.IO.Ports;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace SmartSurgN
{
    public partial class Form1 : Form
    {
        //Backgroundworker that takes care of parsing the file and sending the contents to the device
        //Bg worker for progressbar update
        public static BackgroundWorker MyAsyncTask;

        public int ProgressBarUpdaterStatus = 0;

        //delegate implementation for the first time for setting button text
        delegate void SetButtonTextCallback(string text);
        delegate void SetProgressBarTextCallback(int text);

        //delegate implementation for the first time for setting Label text
        delegate void SetLabelTextCallback(string text);

        static SerialPort _sp;

        private byte[] bytes = new byte[2052];

        public Form1()
        {
            InitializeComponent();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            _serial_load();
        }

        //Function that sets the com port parameters
        void _setComParams(SerialPort _sp, String PortName)
        {
            Console.WriteLine("Setting COM Params");
            //set the port name
            _sp.PortName = PortName;

            //Change the Baud Rate Here --- > 115200 by default
            _sp.BaudRate = 115200;


            //Change the Parity here
            _sp.Parity = 0;

            //Change the No of Stop bits here
            _sp.StopBits = StopBits.One;
            //Change the DataBit count here
            _sp.DataBits = 8;
            //Change the read and write timeouts here
            //ReadTimeout
            _sp.ReadTimeout = 1000;
            //Write Timeout
            _sp.WriteTimeout = 1000;
            Console.WriteLine(_sp.GetType());
        }

        private void cmbxScan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Handle item changed event of combobox
            Console.WriteLine("Selection has been changed by user!!");

            //Assign the new serialport instance to the variable
            //sp-variable that represents a serialport instance

            _sp = new SerialPort();

            //Set the com port params here in the below function
            _setComParams(_sp, cmbxScan.SelectedItem.ToString());
        }

        public void _serial_load()
        {
            try
            {
                cmbxScan.Items.Clear();
                string[] ports = SerialPort.GetPortNames();
                Console.WriteLine("Ports are : " + ports.Length);
                /*ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"].ToString().Contains("(COM"))
                    {
                        Console.WriteLine("serial port : {0}", queryObj["Manufacturer"]);
                    }

                }*/
                /*//create management class
                ManagementClass managementClass = new ManagementClass("Win32_ComputerSystem");
                //collection to store all management class
                ManagementObjectCollection managementObjectCollection = managementClass.GetInstances();
                if (managementObjectCollection.Count != 0)
                {
                    foreach (ManagementObject managementObject in managementClass.GetInstances())
                    {
                        // display general system information
                        Console.WriteLine("\nMachine Make: {0}",
                                          managementObject["Manufacturer"].ToString());
                    }
                }*/
                if (ports.Length == 0)
                {
                    Console.WriteLine("No Ports available");
                }
                else
                {
                    foreach (String port_name in ports)
                    {

                        Console.WriteLine(port_name);
                        cmbxScan.Items.Add(port_name);

                        //stripped_port = port_name.Substring(0, 3);
                    }
                    //sp-variable that represents a serialport instance
                    _sp = new SerialPort();
                    //Set the default selection to the 0th item in the box
                    cmbxScan.SelectedIndex = 0;
                    if (cmbxScan.Items.Count != 0)
                    {
                        if (!cmbxScan.SelectedItem.ToString().Equals("NO PORTS FOUND"))
                        {
                            _setComParams(_sp, cmbxScan.SelectedItem.ToString());
                            cmbxScan.SelectedIndex = 0;
                        }

                    }
                    else
                    {
                        MessageBox.Show("No Ports to configure", "SmartSurgN : SerialPort", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbxScan.Items.Add("NO PORTS FOUND");
                        cmbxScan.SelectedIndex = 0;
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                cmbxScan.Items.Add("NO PORTS FOUND");
                cmbxScan.SelectedIndex = 0;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _serial_load();
        }

        ///Function that takes care of clearing the Tx and Rx Buffers of the serialport
        public static void ClearTxRxBuff()
        {
            if (_sp != null)
            {
                try
                {
                    //Clear the Rx Buffer
                    _sp.DiscardInBuffer();

                    //Clear the Tx buffer
                    _sp.DiscardOutBuffer();


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Serial Port Object is Null");
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                //String folderpath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                //MessageBox.Show(folderpath, "Test", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
                if(radio_air.Checked || radio_eye.Checked || radio_LS.Checked)
                {
                    if (radio_processor1.Checked || radio_processor2.Checked || radio_gui.Checked)
                    {
                        DisableRadio();
                        if (_sp != null)
                        {
                            if (!_sp.IsOpen)
                            {
                                _sp.Open();
                            }
                            //create an instance of backgroundworker and assign it to the ProgressReporter
                            MyAsyncTask = new BackgroundWorker();

                            //Backgroundworker can report progress - if true, reports progress, else doesn't report progress
                            MyAsyncTask.WorkerReportsProgress = true;

                            //Backgroundworker supports cancellation. i.e this can be cancelled anytime once it is started by the user
                            //If this param is true, the worker can be cancelled. 
                            //If false, it cannot be cancelled
                            MyAsyncTask.WorkerSupportsCancellation = true;

                            //Add an instance that has to be executed in the DOWork that runs the background task.
                            MyAsyncTask.DoWork += new DoWorkEventHandler(Background_worker);

                            //Add an instance of ProgressChanged EventHandler to the ProgressChangedEventHandler of the ProgressReporter
                            MyAsyncTask.ProgressChanged += new ProgressChangedEventHandler(BackgroundWorker_Progress);

                            //Add an instance of RunWorkerCompletedHander to the ProgressReporter's same method.
                            MyAsyncTask.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_Completed);

                            //Backgroundworker reports progress - true - > Yes, False -- > No
                            MyAsyncTask.WorkerReportsProgress = true;

                            if (ProgressBarUpdaterStatus == 0)
                            {
                                MyAsyncTask.RunWorkerAsync(argument: "Read");

                                ProgressBarUpdaterStatus = 1;
                            }
                            else
                            {
                                MessageBox.Show("Busy!!", "BootLoader : Background Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                            Console.WriteLine("Started!!");
                        }
                        else
                        {
                            Console.WriteLine("Serial Port is null");
                            EnableRadio();
                            MessageBox.Show("Invalid or Null Serial Port Handler!!", "BootLoader : SerialPort", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select one type", "BootLoader : Type", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Select One SurgN", "BootLoader : SurgN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error occured : " + exception.Message);
            }
        }

        //Background worker that handles the tranfer of commands and response between the application and device
        void Background_worker(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("Inside BG Worker");
            try
            {
                String response = "";
                String type = "";
                String command = (String)e.Argument;
                Console.WriteLine("Process to execute is :" + command);
                if (radio_processor1.Checked)
                {
                    type = "0";
                }
                else if (radio_processor2.Checked)
                {
                    type = "1";
                }
                else if (radio_gui.Checked)
                {
                    type = "2";
                }
                if (command.Equals("Read"))
                {
                    String shut_response_code = _shutCommand(e, type);
                    if (shut_response_code == "Proceed")
                    {
                        String data_to_write = "";
                        String destination = "";
                        if (radio_LS.Checked)
                        {
                            Console.WriteLine("Inside LS");
                            data_to_write = "{4,1,0,9,0}";                        //Read Command for Light Source
                            //data_to_write = "{4,1,0,9,1," + type + "}";         //Read Command for Light Source
                            destination = "1";
                        }
                        else if (radio_air.Checked)
                        {
                            Console.WriteLine("Inside Air");
                            data_to_write = "{4,2,0,9,0}";                        //Read Command for AirSurgN
                            //data_to_write = "{4,2,0,9,1," + type + "}";         //Read Command for AirSurgN
                            destination = "2";
                        }
                        else if (radio_eye.Checked)
                        {
                            Console.WriteLine("Inside Eye");
                            data_to_write = "{4,3,0,9,0}";                        //Read Command for EyeRSurgN
                                                                                  //data_to_write = "{4,3,0,9,1," + type + "}";         //Read Command for EyeRSurgN
                            destination = "3";
                        }
                        Console.WriteLine("Data to write : " + data_to_write);
                        Console.WriteLine("Destination : " + destination);
                        if (!_sp.IsOpen)
                        {
                            Console.WriteLine("Serial Port Not open");
                            _sp.Open();
                        }
                        ClearTxRxBuff();
                        _sp.Write(data_to_write);
                        while (true)
                        {
                            _sp.ReadTimeout = 500;
                            response = _sp.ReadTo("}");
                            Console.WriteLine("Response is : " + response);
                            response = response.Substring(1);
                            String[] splitted_response = response.Split(new String[] { "," }, StringSplitOptions.None);
                            if (splitted_response[1] != "4")
                            {
                                continue;
                            }
                            else
                            {
                                if (!response.Equals(""))
                                {
                                    _processResponse(response, "Read", destination, type);
                                }
                                else
                                {
                                    Console.WriteLine("Length of Indata : " + response.Length);
                                }
                                break;
                            }
                        }
                        e.Result = new string[] { "Read" };
                    }

                }
                else if (command.Equals("Upload"))
                {
                    if (btnStart.Text.Equals("Start"))
                    {
                        if (MyAsyncTask.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        SetButtonText("Cancel");
                        SetLabelText("Uploading");
                        //String folderpath = "C:\\temp\\SSN\\AirSurgN";
                        String folderpath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                        Console.WriteLine("Current Folder Path is : " + folderpath);
                        String filename = "";
                        if (radio_LS.Checked)
                        {
                            if (radio_processor1.Checked)
                            {
                                //File for Light Source
                                //filename = file_to_find("IRLightSourceMainApplication*.bin", folderpath);
                                filename = file_to_find("IRLightSurgNMainApplication*.bin", folderpath);
                            }
                            else if (radio_gui.Checked)
                            {
                                //filename = file_to_find("IRLightSourceImageFile*.bin", folderpath);
                                filename = file_to_find("IRLightSurgNImageFile*.bin", folderpath);
                            }
                        }
                        else if (radio_air.Checked)
                        {
                            //File for AirSurgN
                            if (radio_processor1.Checked)
                            {
                                filename = file_to_find("AirSurgNDisplayMainApplication*.bin", folderpath);
                            }
                            else if (radio_processor2.Checked)
                            {
                                filename = file_to_find("AirSurgNControllerMainApplication*.bin", folderpath);
                            }
                            else if (radio_gui.Checked)
                            {
                                filename = file_to_find("AirSurgNImageFile*.bin", folderpath);
                            }
                        }
                        else if (radio_eye.Checked)
                        {
                            if (radio_processor1.Checked)
                            {
                                //File for EyeRSurgN
                                filename = file_to_find("EyeRSurgNMainApplication*.bin", folderpath);
                            }
                            else if (radio_gui.Checked)
                            {
                                filename = file_to_find("EyeRSurgNImageFile*.bin", folderpath);
                            }
                        }
                        
                        //filename = file_to_find("AirSurgNMainApplication*.bin", folderpath);
                        _upgradeProcess(folderpath, filename, e, type);
                        /*if (radioBank1.Checked == true)
                        {
                            filename = file_to_find("bank1_v*.bin", folderpath);
                            _upgradeProcess(folderpath, filename, e);
                        }
                        else if (radioBank2.Checked == true)
                        {
                            filename = file_to_find("bank2_v*.bin", folderpath);
                            _upgradeProcess(folderpath, filename, e);
                        }
                        else
                        {
                            MessageBox.Show("Select Firmware", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }*/
                        e.Result = new string[] { "Upload" };
                    }
                    else if (btnStart.Text.Equals("Cancel"))
                    {
                        MyAsyncTask.CancelAsync();
                        //SetLabelText("Idle");
                    }
                }

            }
            catch (TimeoutException timeoutException)
            {
                Console.WriteLine("Inside Background Worker : " + timeoutException.Message);
                MessageBox.Show("Timeout Occurs,", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearTxRxBuff();
            }
            catch (InvalidOperationException invalidException)
            {
                Console.WriteLine(invalidException.Message);
                ClearTxRxBuff();
            }
            catch (IOException ioexception)
            {
                Console.WriteLine(ioexception.Message);
                MessageBox.Show("IOException Occurs,", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearTxRxBuff();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        //Update the change with progress in the ProgressBar in the UI
        void BackgroundWorker_Progress(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine(e.ProgressPercentage);
            string[] response = (string[])e.UserState;
            if (response[0].Equals("Read"))
            {
                txtProcessor.Text = response[1];
                txtBank1.Text = response[2];
                //txtBank2.Text = response[3];
            }
            else if (response[0].Equals("ProgressBar"))
            {
                int value = progressBar1.Value;
                Console.WriteLine("Progress bar maximum value is " + progressBar1.Maximum);
                Console.WriteLine("Initial Progress value is : " + value);
                int progress = unchecked((int)(value + int.Parse(response[1])));
                Console.WriteLine("Progress value to update is : " + progress);
                progressBar1.Value = progress;
            }
        }

        //Report to UI, that the backgroundworker has completed it's operation
        void BackgroundWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                Console.WriteLine("Inside Completed");
                string[] response = (string[])e.Result;
                if (response != null)
                {
                    if (response[0].Equals("Read"))
                    {

                    }
                    else if (response[0].Equals("Upload"))
                    {
                        //SetButtonText("Start");
                        //SetLabelText("Idle");
                    }
                }
            }
            catch (Exception Exception)
            {
                Console.WriteLine(Exception.Message);
            }
            EnableRadio();
            SetButtonText("Start");
            SetLabelText("Idle");
            ProgressBarUpdaterStatus = 0;
        }

        //Function that sets text in the button by comapring the thread in which the button was created and the 
        //current thread that is  trying to access the button.
        public void SetButtonText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (btnStart.InvokeRequired)
            {
                SetButtonTextCallback d = new SetButtonTextCallback(SetButtonText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                btnStart.Text = text;
            }
        }

        //Function that sets text in the button by comapring the thread in which the button was created and the 
        //current thread that is  trying to access the button.
        public void ProgressMaximum(int value)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (progressBar1.InvokeRequired)
            {
                SetProgressBarTextCallback d = new SetProgressBarTextCallback(ProgressMaximum);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                progressBar1.Value = 0;
                progressBar1.Maximum = value;
            }
        }

        //Function that processes the incoming data (response)
        //Which command - The response is processed based on this parameter
        static string _processResponse(String response, String Command, String destination, String type)
        {
            try
            {
                //response = response.Substring(1);
                Console.WriteLine("Response is : " + response);
                String[] splitted_response = response.Split(new String[] { "," }, StringSplitOptions.None);
                Console.WriteLine("Splitted Response is : " + splitted_response[0]);
                Console.WriteLine("Destination is : " + destination);
                if (splitted_response[0] == destination)
                {
                    if (Command.Equals("Read"))
                    {
                        Console.WriteLine(response);
                        //response = response.Substring(1);
                        //String[] splitted_response = response.Split(new String[] { "," }, StringSplitOptions.None);
                        if (splitted_response[1] == "4")
                        {
                            if (splitted_response[3] == "9")
                            {
                                //MyAsyncTask.ReportProgress(9, new string[] {"Read",splitted_response[5], splitted_response[6],
                                //    splitted_response[7]});
                                if(type == "0")
                                {
                                    MyAsyncTask.ReportProgress(9, new string[] { "Read", splitted_response[5], splitted_response[8] });     //Processor 1
                                } else if(type == "1")
                                {
                                    MyAsyncTask.ReportProgress(9, new string[] { "Read", splitted_response[6], splitted_response[8] });     //Processor 2
                                }
                                else if (type == "2")
                                {
                                    MyAsyncTask.ReportProgress(9, new string[] { "Read", splitted_response[7], splitted_response[8] });     //GUI
                                } else
                                {

                                }
                                //MyAsyncTask.ReportProgress(9, new string[] { "Read", splitted_response[5], splitted_response[6] });

                            }
                            else
                            {
                                Console.WriteLine("Command id mismatch");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Address Mismatch");
                        }
                    }
                    else if (Command.Equals("Packet Size"))
                    {
                        response = response.Substring(1);
                        //String[] splitted_response = response.Split(new String[] { "," }, StringSplitOptions.None);
                        if (splitted_response[1] == "4")
                        {
                            if (splitted_response[3] == "10")
                            {
                                return splitted_response[5];
                            }
                            else
                            {
                                Console.WriteLine("Command id mismatch");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Address Mismatch");
                        }
                    }
                    else if (Command.Equals("Upload Response"))
                    {
                        response = response.Substring(1);
                        //String[] splitted_response = response.Split(new String[] { "," }, StringSplitOptions.None);
                        if (splitted_response[3] == "11")
                        {
                            if (splitted_response[5] == "0")
                            {
                                if(splitted_response[6] == type)
                                {
                                    return "Next";
                                } else
                                {
                                    return "Retain";
                                }
                            }
                            else
                            {
                                return "Retain";
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Command ID");
                        }
                    }
                    else if (Command.Equals("Shut"))
                    {
                        response = response.Substring(1);
                        //String[] splitted_response = response.Split(new String[] { "," }, StringSplitOptions.None);
                        if (splitted_response[1] == "4")
                        {
                            if (splitted_response[3] == "8")
                            {
                                if (splitted_response[5] == "1")
                                {
                                    return "Proceed";
                                }
                                else
                                {
                                    Console.WriteLine("Stop");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid Command ID");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Address Mismatch");
                        }
                    }
                }
                else
                {
                    if (destination == "1")
                    {
                        Console.WriteLine("Response from Light Source received");
                    }
                    else if (destination == "2")
                    {
                        Console.WriteLine("Response from AirSurgN received");
                    }
                    else if (destination == "3")
                    {
                        Console.WriteLine("Response from EyeRSurgN received");
                    }
                    return "Wrong destination";
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                ClearTxRxBuff();
            }
            return "";
        }

        string _shutCommand(DoWorkEventArgs e,String type)
        {
            string packet_to_send = "";
            String destination = "";
            if (radio_LS.Checked)
            {
                packet_to_send = "{4,1,0,8,1,1}";         //Shut Command for Light Source
                destination = "1";
            }
            else if (radio_air.Checked)
            {
                packet_to_send = "{4,2,0,8,1,1}";         //Shut Command for AirSurgN
                destination = "2";
            }
            else if (radio_eye.Checked)
            {
                packet_to_send = "{4,3,0,8,1,1}";         //Shut Command for EyeRSurgN
                destination = "3";
            }
            //string packet_to_send = "{4,2,0,10,2,";
            for (int count=0; count < 3; count++)
            {
                Console.WriteLine("Packet To send : " + packet_to_send);
                ClearTxRxBuff();
                int number = _sp.BytesToRead;
                Console.WriteLine("Bytes Number is : " + number);
                _sp.Write(packet_to_send);

                _sp.ReadTimeout = 500;
                String shut_response = _sp.ReadTo("}");
                Console.WriteLine("Response for Shutdown : " + shut_response);
                shut_response = shut_response.Substring(1);
                String[] splitted_response = shut_response.Split(new String[] { "," }, StringSplitOptions.None);
                if (splitted_response[1] != "4")
                {
                    continue;
                }
                else
                {
                    string shut_response_code = _processResponse(shut_response, "Shut", destination, type);
                    return shut_response_code;
                }
            }
            return "";

        }

        string _shutOpenCommand(DoWorkEventArgs e, String type)
        {
            string packet_to_send = "";
            String destination = "";
            if (radio_LS.Checked)
            {
                packet_to_send = "{4,1,0,8,1,0}";         //Shut Open Command for Light Source
                destination = "1";
            }
            else if (radio_air.Checked)
            {
                packet_to_send = "{4,2,0,8,1,0}";         //Shut Open Command for AirSurgN
                destination = "2";
            }
            else if (radio_eye.Checked)
            {
                packet_to_send = "{4,3,0,8,1,0}";         //Shut Open Command for EyeRSurgN
                destination = "3";
            }
            //string packet_to_send = "{4,2,0,10,2,";
            for (int count = 0; count < 3; count++)
            {
                Console.WriteLine("Packet To send : " + packet_to_send);
                ClearTxRxBuff();
                int number = _sp.BytesToRead;
                Console.WriteLine("Bytes Number is : " + number);
                _sp.Write(packet_to_send);

                _sp.ReadTimeout = 500;
                String shut_response = _sp.ReadTo("}");
                Console.WriteLine("Response for Shutdown : " + shut_response);
                shut_response = shut_response.Substring(1);
                String[] splitted_response = shut_response.Split(new String[] { "," }, StringSplitOptions.None);
                if (splitted_response[1] != "4")
                {
                    continue;
                }
                else
                {
                    string shut_response_code = _processResponse(shut_response, "Shut", destination, type);
                    return shut_response_code;
                }
            }
            return "";

        }

        string _upgradeProcess(string folderpath, string filename, DoWorkEventArgs e, String type)
        {
            try
            {
                if (MyAsyncTask.CancellationPending)
                {
                    e.Cancel = true;
                    return "Cancel";
                }
                if (!File.Exists(folderpath + "\\" + filename))
                {
                    MessageBox.Show("Bin file not available in default folder", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    String shut_response_code = _shutCommand(e, type);
                    if (shut_response_code == "Proceed")
                    {
                        string packet_to_send = "";
                        String destination = "";
                        if (radio_LS.Checked)
                        {
                            packet_to_send = "{4,1,0,10,3," + type + ",";         //Upload Command for Light Source
                            destination = "1";
                        }
                        else if (radio_air.Checked)
                        {
                            packet_to_send = "{4,2,0,10,3," + type + ",";         //Upload Command for AirSurgN
                            destination = "2";
                        }
                        else if (radio_eye.Checked)
                        {
                            packet_to_send = "{4,3,0,10,3," + type + ",";         //Upload Command for EyeRSurgN
                            destination = "3";
                        }
                        //packet_to_send = "{4,2,0,10,2," + type + ",";
                        /*string version = filename.Substring(28, 5);
                        Console.WriteLine(version);
                        packet_to_send += version + ",";*/

                        var fileSize = (new FileInfo(folderpath + "\\" + filename).Length);
                        Console.WriteLine(fileSize);
                        //string hexValue = fileSize.ToString("X4");

                        ProgressMaximum(unchecked((int)fileSize));
                        //progressBar1.Maximum = unchecked((int)fileSize);

                        string version = "";
                        //IRLightSurgNMainApplication2.6.6.bin
                        //IRLightSurgNImagrFile2.6.6.bin
                        //EyeRSurgNMainApplication2.6.6.bin
                        //AirSurgNControllerMainApplication
                        //AirSurgNDisplayMainApplication
                        if (radio_LS.Checked)
                        {
                            if (radio_processor1.Checked)
                            {
                                version = filename.Substring(27, 5);
                            }
                            else if (radio_gui.Checked)
                            {
                                version = filename.Substring(21, 5);
                            }
                        }
                        else if (radio_eye.Checked)
                        {
                            if (radio_processor1.Checked)
                            {
                                version = filename.Substring(24, 5);
                            }
                            else if (radio_gui.Checked)
                            {
                                version = filename.Substring(18, 5);
                            }
                        }
                        else if (radio_air.Checked)
                        {
                            if (radio_processor1.Checked)
                            {
                                version = filename.Substring(30, 5);
                            }
                            else if (radio_processor2.Checked)
                            {
                                version = filename.Substring(33, 5);
                            }
                            else if (radio_gui.Checked)
                            {
                                //AirSurgNImageFile2.0.0
                                version = filename.Substring(17, 5);
                            }
                        }

                        //string version = filename.Substring(23, 5);
                        Console.WriteLine(version);

                        packet_to_send += fileSize + "," + version + "}";
                        Console.WriteLine("Packet To send : " + packet_to_send);

                        ClearTxRxBuff();
                        _sp.Write(packet_to_send);
                        _sp.ReadTimeout = 500;
                        String indata = _sp.ReadTo("}");
                        indata = indata.Substring(1);
                        Console.WriteLine("Response for Size : " + indata);
                        string return_code = _processResponse(indata, "Packet Size", destination, type);
                        if (return_code == "1")
                        {
                            Console.WriteLine("Return code is 1");
                            byte[] fileBytes = File.ReadAllBytes(folderpath + "\\" + filename);
                            Console.WriteLine("File Size is : " + fileBytes.Length);
                            _upload(folderpath + "\\" + filename, fileBytes, 2048, e, destination);
                        }
                        else if (return_code == "0")
                        {
                            MessageBox.Show("Device not ready to program", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return "Device Not Ready";
                        }
                    }
                    else
                    {
                        MessageBox.Show("Device Shutdown fail", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return "Device Not Ready";
                    }   
                }
            }
            catch (InvalidOperationException invalidException)
            {
                Console.WriteLine(invalidException.Message);
                ClearTxRxBuff();
            }
            catch (TimeoutException timeoutException)
            {
                Console.WriteLine("Inside Upgrade Process : " + timeoutException.Message);
                ClearTxRxBuff();
                MessageBox.Show("Timeout Occurs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return "";
        }

        string _upload(string filename, byte[] fileBytes, int no_of_bytes_cycle, DoWorkEventArgs e, String destination)
        {
            try
            {
                if (MyAsyncTask.CancellationPending)
                {
                    e.Cancel = true;
                    return "Cancel";
                }
                using (FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    //Loop count to send a full file
                    float file_cycle = (int)(fileStream.Length / no_of_bytes_cycle);
                    //Progress Bar incrementing value
                    float progress_update = (int)progressBar1.Maximum / file_cycle;

                    Console.WriteLine("Progress Bar Update cycle is : " + progress_update);

                    int round = 1;
                    int bytes_in_buffer = fileStream.Read(bytes, 0, no_of_bytes_cycle);

                    while (bytes_in_buffer > 0)
                    {
                        try
                        {
                            if (MyAsyncTask.CancellationPending)
                            {
                                e.Cancel = true;
                                return "Cancel";
                            }
                            Console.WriteLine("No of bytes read at this shot : " + bytes_in_buffer);

                            if (bytes_in_buffer == 0)
                            {
                                MessageBox.Show("Reached EOF!!. Halting Now", "BootLoader : Firmware Upgrade", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Console.WriteLine("Reached End Of File!!");
                                break;
                            }

                            if (!_sp.IsOpen)
                            {
                                _sp.Open();
                            }

                            //byte[] bytes_to_send = checksum_cal(bytes, round);
                            checksum_cal(bytes_in_buffer, round);

                            ClearTxRxBuff();
                            Console.WriteLine("Before Write function");
                            //Console.WriteLine(Encoding.Default.GetString(bytes_to_send));
                            int offset = bytes_in_buffer + 4;
                            Console.WriteLine("Bytes length to send " + offset);
                            Console.WriteLine("Count : " + bytes[bytes_in_buffer].ToString("X"));
                            Console.WriteLine("Checksum MSB : " + bytes[bytes_in_buffer + 2].ToString("X"));
                            Console.WriteLine("Last Byte : " + bytes[bytes_in_buffer + 3].ToString("X"));

                            _sp.Write(bytes, 0, offset);
                            Console.WriteLine("After Write function");
                            if(offset < 2052)
                            {
                                int mod = offset % 64;
                                if(mod == 0)
                                {
                                    _sp.Write("\r");
                                    Console.WriteLine("Empty written to serial port");
                                }
                            }
                            _sp.ReadTimeout = 15000;
                            string response = _sp.ReadTo("}");
                            Console.WriteLine("Response is : " + response);
                            response = response.Substring(1);
                            string return_code = _processResponse(response, "Upload Response", destination, round.ToString());
                            if(return_code == "Next")
                            {
                                MyAsyncTask.ReportProgress(10, new string[] { "ProgressBar", bytes_in_buffer.ToString() });
                                Array.Clear(bytes, 0, bytes.Length);
                                Console.WriteLine("Before File Read");
                                bytes_in_buffer = fileStream.Read(bytes, 0, no_of_bytes_cycle);

                                Console.WriteLine("File Readed successfully");
                                round = round + 1;
                                Console.WriteLine("Set No : " + round.ToString());
                                Console.WriteLine("\r\n====================================================END==================================================\r\n");
                            } else if(return_code == "Retain")
                            {
                                Console.WriteLine("Resending same bytes again");
                            }
                        }
                        catch (TimeoutException timeoutException)
                        {
                            Console.WriteLine(timeoutException.Message);
                            /*MyAsyncTask.ReportProgress(10, new string[] { "ProgressBar", bytes_in_buffer.ToString() });
                            Console.WriteLine("Before File Read");
                            bytes_in_buffer = fileStream.Read(bytes, 0, no_of_bytes_cycle);

                            Console.WriteLine("File Readed successfully");
                            round = round + 1;
                            Console.WriteLine("Set No : " + round.ToString());
                            Console.WriteLine("\r\n====================================================END==================================================\r\n");*/
                            MessageBox.Show("Device response Timeout", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine("Inside general exception");
                            Console.WriteLine(exception.Message);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            return "";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (radio_air.Checked || radio_eye.Checked || radio_LS.Checked)
                {
                    if (radio_processor1.Checked || radio_processor2.Checked || radio_gui.Checked)
                    {
                        DisableRadio();
                        if (_sp != null)
                        {
                            if (!_sp.IsOpen)
                            {
                                _sp.Open();
                            }
                            //create an instance of backgroundworker and assign it to the ProgressReporter
                            MyAsyncTask = new BackgroundWorker();

                            //Backgroundworker can report progress - if true, reports progress, else doesn't report progress
                            MyAsyncTask.WorkerReportsProgress = true;

                            //Backgroundworker supports cancellation. i.e this can be cancelled anytime once it is started by the user
                            //If this param is true, the worker can be cancelled. 
                            //If false, it cannot be cancelled
                            MyAsyncTask.WorkerSupportsCancellation = true;

                            //Add an instance that has to be executed in the DOWork that runs the background task.
                            MyAsyncTask.DoWork += new DoWorkEventHandler(Background_worker);

                            //Add an instance of ProgressChanged EventHandler to the ProgressChangedEventHandler of the ProgressReporter
                            MyAsyncTask.ProgressChanged += new ProgressChangedEventHandler(BackgroundWorker_Progress);

                            //Add an instance of RunWorkerCompletedHander to the ProgressReporter's same method.
                            MyAsyncTask.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_Completed);

                            //Backgroundworker reports progress - true - > Yes, False -- > No
                            MyAsyncTask.WorkerReportsProgress = true;

                            if (ProgressBarUpdaterStatus == 0)
                            {
                                MyAsyncTask.RunWorkerAsync(argument: "Upload");

                                ProgressBarUpdaterStatus = 1;
                            }
                            else
                            {
                                //MessageBox.Show("Busy!!", "BootLoader : Background Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                MyAsyncTask.CancelAsync();

                            }
                            Console.WriteLine("Started!!");
                        }
                        else
                        {
                            Console.WriteLine("Serial Port is null");
                            MessageBox.Show("Invalid or Null Serial Port Handler!!", "BootLoader : SerialPort", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            EnableRadio();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select one type", "BootLoader : Type", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Select One SurgN", "BootLoader : SurgN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (FileNotFoundException file_exception)
            {
                Console.WriteLine("File not found");
            }
        }

        //Function that sets text in the button by comparing the thread in which the label was created and the 
        //current thread that is trying to access the label
        private void SetLabelText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (lblStatus.InvokeRequired)
            {
                SetLabelTextCallback d = new SetLabelTextCallback(SetLabelText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                lblStatus.Text = text;
            }
        }

        public string file_to_find(String file_startswith, string folderpath)
        {
            System.IO.DirectoryInfo directoryInfo = new DirectoryInfo(folderpath);
            FileInfo[] fileInfos = directoryInfo.GetFiles(file_startswith);
            Console.WriteLine("################ File name is : #################");
            foreach (FileInfo file in fileInfos)
            {
                Console.WriteLine("################ File name is : " + file.Name + "#################");
                if (file.Name.Equals(""))
                {
                    return "FILENOTPRESENT";
                }
                else
                {
                    return file.Name;
                }
            }
            return "FILENOTPRESENT";
        }
        public byte[] checksum_cal(int bytes_in_buffer, int round)
        {
            int checksum = 0;
            for (int count = 0; count < bytes_in_buffer; count++)
            {
                checksum += bytes[count];
            }

            int round_value = (round >> 8) & 0xff;
            bytes[bytes_in_buffer] = (byte)round_value;
            Console.WriteLine("Round Value is " + round_value.ToString("X"));

            int round_value1 = round & 0xff;
            bytes[bytes_in_buffer+1] = (byte)round_value1;
            Console.WriteLine("Round Value 1 is " + round_value1.ToString("X"));

            Console.WriteLine("Round Is " + round);
            checksum += round_value;
            checksum += round_value1;

            checksum = ~checksum;
            checksum = checksum + 1;
            checksum = checksum & 0xffff;
            Console.WriteLine("checksum is " + checksum.ToString("X4"));

            int value = (checksum >> 8) & 0xff;
            bytes[bytes_in_buffer + 2] = (byte)value;
            Console.WriteLine("Value is " + value.ToString("X"));

            int value1 = checksum & 0xff;
            bytes[bytes_in_buffer + 3] = (byte)value1;
            Console.WriteLine("Value1 is " + value1.ToString("X"));
            return bytes;
        }

        private void radio_LS_CheckedChanged(object sender, EventArgs e)
        {
            radio_processor1.Enabled = true;
            radio_processor2.Enabled = false;
            radio_gui.Enabled = true;

            radio_processor1.Checked = false;
            radio_processor2.Checked = false;
            radio_gui.Checked = false;
        }

        private void radio_eye_CheckedChanged(object sender, EventArgs e)
        {
            radio_processor1.Enabled = true;
            radio_processor2.Enabled = false;
            radio_gui.Enabled = true;

            radio_processor1.Checked = false;
            radio_processor2.Checked = false;
            radio_gui.Checked = false;
        }

        private void radio_air_CheckedChanged(object sender, EventArgs e)
        {
            radio_processor1.Enabled = true;
            radio_processor2.Enabled = true;
            radio_gui.Enabled = true;

            radio_processor1.Checked = false;
            radio_processor2.Checked = false;
            radio_gui.Checked = false;
        }

        public void DisableRadio()
        {
            radio_LS.Enabled = false;
            radio_eye.Enabled = false;
            radio_air.Enabled = false;
            radio_processor1.Enabled = false;
            radio_processor2.Enabled = false;
            radio_gui.Enabled = false;
        }

        public void EnableRadio()
        {
            radio_LS.Enabled = true;
            radio_eye.Enabled = true;
            radio_air.Enabled = true;
            
            radio_LS.Checked = false;
            radio_eye.Checked = false;
            radio_air.Checked = false;

            radio_processor1.Checked = false;
            radio_processor2.Checked = false;
            radio_gui.Checked = false;

            radio_processor1.Enabled = false;
            radio_processor2.Enabled = false;
            radio_gui.Enabled = false;
        }
    }
}
