﻿namespace SmartSurgN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.cmbxScan = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRead = new System.Windows.Forms.Button();
            this.txtBank1 = new System.Windows.Forms.TextBox();
            this.txtProcessor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radio_gui = new System.Windows.Forms.RadioButton();
            this.radio_processor2 = new System.Windows.Forms.RadioButton();
            this.radio_processor1 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radio_air = new System.Windows.Forms.RadioButton();
            this.radio_eye = new System.Windows.Forms.RadioButton();
            this.radio_LS = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRefresh);
            this.groupBox1.Controls.Add(this.cmbxScan);
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(353, 65);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ComPorts";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Location = new System.Drawing.Point(198, 20);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(97, 28);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // cmbxScan
            // 
            this.cmbxScan.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxScan.FormattingEnabled = true;
            this.cmbxScan.Items.AddRange(new object[] {
            "COM1"});
            this.cmbxScan.Location = new System.Drawing.Point(52, 20);
            this.cmbxScan.Name = "cmbxScan";
            this.cmbxScan.Size = new System.Drawing.Size(96, 27);
            this.cmbxScan.TabIndex = 0;
            this.cmbxScan.SelectedIndexChanged += new System.EventHandler(this.cmbxScan_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRead);
            this.groupBox2.Controls.Add(this.txtBank1);
            this.groupBox2.Controls.Add(this.txtProcessor);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(9, 224);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(353, 83);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Version";
            // 
            // btnRead
            // 
            this.btnRead.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.btnRead.Location = new System.Drawing.Point(260, 17);
            this.btnRead.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(82, 47);
            this.btnRead.TabIndex = 7;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // txtBank1
            // 
            this.txtBank1.Location = new System.Drawing.Point(104, 42);
            this.txtBank1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtBank1.Name = "txtBank1";
            this.txtBank1.ReadOnly = true;
            this.txtBank1.Size = new System.Drawing.Size(132, 20);
            this.txtBank1.TabIndex = 5;
            this.txtBank1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtProcessor
            // 
            this.txtProcessor.Location = new System.Drawing.Point(15, 42);
            this.txtProcessor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProcessor.Name = "txtProcessor";
            this.txtProcessor.ReadOnly = true;
            this.txtProcessor.Size = new System.Drawing.Size(68, 20);
            this.txtProcessor.TabIndex = 4;
            this.txtProcessor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(148, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "DeviceID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Processor";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(15, 24);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(327, 25);
            this.progressBar1.TabIndex = 2;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(13, 52);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(24, 13);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "Idle";
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Calibri Light", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(115, 71);
            this.btnStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(113, 42);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnStart);
            this.groupBox3.Controls.Add(this.lblStatus);
            this.groupBox3.Controls.Add(this.progressBar1);
            this.groupBox3.Location = new System.Drawing.Point(9, 312);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(352, 128);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Upload";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radio_gui);
            this.groupBox4.Controls.Add(this.radio_processor2);
            this.groupBox4.Controls.Add(this.radio_processor1);
            this.groupBox4.Location = new System.Drawing.Point(9, 152);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(353, 67);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Type";
            // 
            // radio_gui
            // 
            this.radio_gui.AutoSize = true;
            this.radio_gui.Enabled = false;
            this.radio_gui.Location = new System.Drawing.Point(260, 28);
            this.radio_gui.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radio_gui.Name = "radio_gui";
            this.radio_gui.Size = new System.Drawing.Size(44, 17);
            this.radio_gui.TabIndex = 2;
            this.radio_gui.TabStop = true;
            this.radio_gui.Text = "GUI";
            this.radio_gui.UseVisualStyleBackColor = true;
            // 
            // radio_processor2
            // 
            this.radio_processor2.AutoSize = true;
            this.radio_processor2.Enabled = false;
            this.radio_processor2.Location = new System.Drawing.Point(137, 28);
            this.radio_processor2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radio_processor2.Name = "radio_processor2";
            this.radio_processor2.Size = new System.Drawing.Size(81, 17);
            this.radio_processor2.TabIndex = 1;
            this.radio_processor2.TabStop = true;
            this.radio_processor2.Text = "Processor 2";
            this.radio_processor2.UseVisualStyleBackColor = true;
            // 
            // radio_processor1
            // 
            this.radio_processor1.AutoSize = true;
            this.radio_processor1.Enabled = false;
            this.radio_processor1.Location = new System.Drawing.Point(16, 28);
            this.radio_processor1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radio_processor1.Name = "radio_processor1";
            this.radio_processor1.Size = new System.Drawing.Size(81, 17);
            this.radio_processor1.TabIndex = 0;
            this.radio_processor1.TabStop = true;
            this.radio_processor1.Text = "Processor 1";
            this.radio_processor1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radio_air);
            this.groupBox5.Controls.Add(this.radio_eye);
            this.groupBox5.Controls.Add(this.radio_LS);
            this.groupBox5.Location = new System.Drawing.Point(9, 80);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox5.Size = new System.Drawing.Size(353, 67);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "SurgN";
            // 
            // radio_air
            // 
            this.radio_air.AutoSize = true;
            this.radio_air.Location = new System.Drawing.Point(260, 28);
            this.radio_air.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radio_air.Name = "radio_air";
            this.radio_air.Size = new System.Drawing.Size(67, 17);
            this.radio_air.TabIndex = 2;
            this.radio_air.TabStop = true;
            this.radio_air.Text = "AirSurgN";
            this.radio_air.UseVisualStyleBackColor = true;
            this.radio_air.CheckedChanged += new System.EventHandler(this.radio_air_CheckedChanged);
            // 
            // radio_eye
            // 
            this.radio_eye.AutoSize = true;
            this.radio_eye.Location = new System.Drawing.Point(137, 28);
            this.radio_eye.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radio_eye.Name = "radio_eye";
            this.radio_eye.Size = new System.Drawing.Size(81, 17);
            this.radio_eye.TabIndex = 1;
            this.radio_eye.TabStop = true;
            this.radio_eye.Text = "EyeRSurgN";
            this.radio_eye.UseVisualStyleBackColor = true;
            this.radio_eye.CheckedChanged += new System.EventHandler(this.radio_eye_CheckedChanged);
            // 
            // radio_LS
            // 
            this.radio_LS.AutoSize = true;
            this.radio_LS.Location = new System.Drawing.Point(16, 28);
            this.radio_LS.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radio_LS.Name = "radio_LS";
            this.radio_LS.Size = new System.Drawing.Size(85, 17);
            this.radio_LS.TabIndex = 0;
            this.radio_LS.TabStop = true;
            this.radio_LS.Text = "Light Source";
            this.radio_LS.UseVisualStyleBackColor = true;
            this.radio_LS.CheckedChanged += new System.EventHandler(this.radio_LS_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(334, 442);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "v2.1.5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 458);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "BoatLoader";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbxScan;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.TextBox txtBank1;
        private System.Windows.Forms.TextBox txtProcessor;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radio_gui;
        private System.Windows.Forms.RadioButton radio_processor2;
        private System.Windows.Forms.RadioButton radio_processor1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radio_air;
        private System.Windows.Forms.RadioButton radio_eye;
        private System.Windows.Forms.RadioButton radio_LS;
        private System.Windows.Forms.Label label3;
    }
}

